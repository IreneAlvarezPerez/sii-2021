# Indicamos el nombre que queremos que tenga nuestro proyecto
PROJECT(JuegoDados)

# Indicamos que el proyecto va a crear una librer�a
# e indicamos los archivos que van a dar lugar a la libreria
# Deben estar creados antes de ejecutar CMake
ADD_EXECUTABLE(JuegoDados

	Dado.cpp
	JuegoDados.cpp
	principaljuegodados.cpp

)

TARGET_LINK_LIBRARIES(JuegoDados)

CMAKE_MINIMUM_REQUIRED(VERSION 2.6)
