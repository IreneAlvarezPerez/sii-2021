#ifndef _INC_JUEGODADOS_
#define _INC_JUEGODADOS_

#include "Dado.h"

class JuegoDados 
{
public:

	bool jugar();

private:
	Dado dado1;
	Dado dado2;

};

#endif 

