// Compilar con opción -lpthread:
// $ gcc consumidor.c -lpthread -o consumidor
// ./consumidor
// Ver los semáforos en el directorio /dev/shm/

#include <sys/mman.h>
#include <stdio.h>
#include <semaphore.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>

#define MAX_BUFFER 1024 		/* tamaño del buffer */
#define DATOS_A_PRODUCIR 100000		/* datos a producir */

sem_t *huecos;
sem_t *elementos;
int *buffer; /* buffer de números enteros */

void consumidor(void);

int main(void){
	int fd;
	
	/* se abren los semáforos */
	huecos = sem_open("HUECOS", O_CREAT, 0700, MAX_BUFFER);
	elementos = sem_open("ELEMENTOS", O_CREAT, 0700, 0);
	if (huecos == SEM_FAILED || elementos == SEM_FAILED) {
	  perror("Error en sem_open");
	  return 1;
	}

	/* se abre el segmento de memoria compartida utilizado como buffer circular */
	fd = open("/tmp/buffer", O_CREAT|O_RDONLY, 0700);
	if (fd == -1) {
	  perror("Error en Open");
	  return 1;
	}
	buffer = (int *)mmap(NULL, MAX_BUFFER*sizeof(int), PROT_READ,MAP_SHARED, fd, 0);
	if (buffer == NULL) {
	  perror("Error en mmap");
	  return 1;
	}

	consumidor(); /* se ejecuta el código del consumidor */

	/* se desproyecta el buffer */
	munmap(buffer, MAX_BUFFER*sizeof(int));
	close(fd);

	/* se cierran semáforos */
	sem_close(huecos);
	sem_close(elementos);
	sem_unlink("HUECOS");
	sem_unlink("ELEMENTOS");	
	return 0;
}

/* código del proceso consumidor */
void consumidor(void){
	int dato;	/* dato a consumir */
	int posicion = 0; /* posición que indica el elemento a extraer */
	int j;

	for (j=0; j<DATOS_A_PRODUCIR; j++) {
	  dato = j;
	  sem_wait(elementos); 		/* un elemento menos */
	  dato = buffer[posicion];
	  posicion=(posicion+1) % MAX_BUFFER; /* nueva posición */
	  sem_post(huecos);	/* un hueco más */
	  printf ("Dato consumido = %d\n", dato);
	}
	return;
}


