#include <stdio.h>
#include <sys/time.h> 
#include <sys/types.h>    
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

int main(void)
{
  struct sockaddr_in server_addr,client_addr;
  int sd, size;
  int byte;

  sd = socket(AF_INET, SOCK_DGRAM, 0);
  if (sd < 0) {
    printf("Error en la llamada socket\n");
    return 1;
  }
  
  bzero((char *)&server_addr, sizeof(server_addr));
  server_addr.sin_family = AF_INET;
  server_addr.sin_addr.s_addr = INADDR_ANY;
  server_addr.sin_port = htons(4200);

  int on=1;
  setsockopt(sd,SOL_SOCKET,SO_REUSEADDR, &on, sizeof(on));

  if (bind(sd, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0){
    printf("Error: llamada bind\n");
    return 1;
  }

  size = sizeof(client_addr);
 
  while (1){
    /* recibe el dato del cliente*/
    recvfrom(sd, &byte, 1, 0, (struct sockaddr *)&client_addr, &size);

    printf("IP del cliente: %s, dato: %c\n", inet_ntoa(client_addr.sin_addr), byte);

    /* se lo devuelve al cliente */
    sendto(sd, &byte, sizeof(int), 0, (struct sockaddr *)&client_addr, size);
      
  }
  close (sd);
  return 0;
}


