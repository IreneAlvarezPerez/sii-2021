#ifndef _INC_LISTA_PASAJEROS
#define _INC_LISTA_PASAJEROS

#include <vector>
#include "Pasajero.h"

class ListaPasajeros
{
public:
	void setDatosPasajero (const Pasajero p1)
	{laListaPasajeros.push_back(p1);}
	int darNumeroPasajeros ( void ) const
	{ return (laListaPasajeros.size()); }
	Pasajero  getDatoPasajero( unsigned numPasajero )
	{ return laListaPasajeros[numPasajero];}


private:
	std::vector<Pasajero> laListaPasajeros;
};

#endif
