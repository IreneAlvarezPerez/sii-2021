#ifndef _PASAJERO_INC_
#define _PASAJERO_INC_

#include <string>

class Pasajero 
{
public:
	void setNombre(const char *nom)
	{nombre = nom;}
	std::string & getNombre( void ) 
		{return  nombre;}
	void setPasaporte(unsigned long pasaporte)
	{numPasaporte=pasaporte;}
	unsigned long getPasaporte( void ) const
	{return (numPasaporte);}

private:
	std::string nombre;
	unsigned long numPasaporte;
};

#endif 

