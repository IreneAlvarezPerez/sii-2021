# Indicamos el nombre que queremos que tenga nuestro proyecto
PROJECT(Pizarra)

# Indicamos que el proyecto va a crear una librer�a
# e indicamos los archivos que van a dar lugar a la libreria
# Deben estar creados antes de ejecutar CMake
ADD_EXECUTABLE(Pizarra
	pizarra.cpp
)

TARGET_LINK_LIBRARIES(Pizarra)

CMAKE_MINIMUM_REQUIRED(VERSION 2.6)
