// Urbanizacion.h: interface for the Urbanizacion class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_URBANIZACION_H__0CD0C450_A5B8_467A_861F_45D55CE011D7__INCLUDED_)
#define AFX_URBANIZACION_H__0CD0C450_A5B8_467A_861F_45D55CE011D7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <vector>
#include "Casa.h"

class Urbanizacion  
{
	unsigned filasCasa, columnasCasa;
	std::vector<Casa> laListaCasas;

public:

	Urbanizacion(unsigned, unsigned);
	virtual ~Urbanizacion();
	void dibuja();

};

#endif // !defined(AFX_URBANIZACION_H__0CD0C450_A5B8_467A_861F_45D55CE011D7__INCLUDED_)
