// Urbanizacion.cpp: implementation of the Urbanizacion class.
//
//////////////////////////////////////////////////////////////////////

#include "..\..\INCLUDES\DOMINIO\Urbanizacion.h"
#include "..\..\INCLUDES\comunes\glut.h"

#define ANCHO_BLOQUE	1.0f
#define ALTO_BLOQUE		0.5f    
#define ANCHO_TECHO		1.0f
#define SEPARACION_CASA	3.0f

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Urbanizacion::Urbanizacion(unsigned filas, unsigned columnas)
{
	filasCasa = filas; columnasCasa = columnas;
	for(unsigned i = 0; i<filas; i++)
		for(unsigned j = 0; j<columnas; j++)
		{
			this->laListaCasas.push_back(Casa(ANCHO_BLOQUE,ALTO_BLOQUE,ANCHO_TECHO));
			this->laListaCasas[(i*columnas)+j].setPosicion(SEPARACION_CASA*j,0,SEPARACION_CASA*i);
		}
}

Urbanizacion::~Urbanizacion()
{

}

void Urbanizacion::dibuja()
{
	float x_ojo=10;
	float y_ojo=7.5;
	float z_ojo=40;

	gluLookAt(x_ojo, y_ojo, z_ojo,  // posicion del ojo
			0.0, y_ojo, 0.0,      // hacia que punto mira  (0,0,0) 
			0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	for (unsigned i=0;i<filasCasa*columnasCasa; i++)
		this->laListaCasas[i].dibuja();
}