// Moneda.cpp: implementation of the Moneda class.
//
//////////////////////////////////////////////////////////////////////

#include "../../cabeceras/comunes/Moneda.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Moneda::Moneda(unsigned cantidad):Dinero(cantidad)
{
	if(cantidad == 1)
		this->elTipoMoneda = UN_CENT;
	else if (cantidad ==2)
		this->elTipoMoneda = DOS_CENT;
	else if (cantidad == 5)
		this->elTipoMoneda = CINCO_CENT;
	else if(cantidad == 10)
		this->elTipoMoneda = DIEZ_CENT;
	else if (cantidad ==20)
		this->elTipoMoneda = VEINTE_CENT;
	else if (cantidad == 50)
		this->elTipoMoneda = CINCUENTA_CENT;
	else if (cantidad == 100)
		this->elTipoMoneda = UNO;
	else if (cantidad == 200)
		this->elTipoMoneda = DOS;

	else
		; //Error
}

Moneda::~Moneda()
{

}
