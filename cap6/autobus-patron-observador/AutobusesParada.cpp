#include <iostream>
#include <string>
#include <vector>
#include "Observable.h"
using namespace std;

class Parada {
  unsigned numParada;
  vector<unsigned> conexionesBuses;
//  Argument arg;


public:

  // Clase interna:
  class NotificadorBuses;
  friend class Parada::NotificadorBuses;
  class NotificadorBuses : public Observable {
    Parada* pParada;
	Argument arg;

  public:
    NotificadorBuses(Parada* f) : pParada(f){}
//    void notifyObservers(Argument* arg = 0) {
	void notifyObservers() {
		arg.nParada = pParada->numParada;
		arg.conexiones = pParada->conexionesBuses;

		setChanged();
        Observable::notifyObservers(&arg);
      }
  } elNotificardorLineasBuses;

  Parada(unsigned numP, unsigned numC, unsigned *pConexiones ) : numParada(numP), elNotificardorLineasBuses(this) {
	  for(unsigned i=0;i<numC;i++)
		  conexionesBuses.push_back(pConexiones[i]);
//	  arg.nParada = numParada;
//	  arg.conexiones = conexionesBuses;

  }
  void cambiosEnParada() { // Modificaci�n en la parada
//    elNotificardorLineasBuses.notifyObservers(&arg);
    elNotificardorLineasBuses.notifyObservers();
  }

};

class Bus {
  unsigned numLinea;
  // Clase interna para observar las paradas
  class ObservadorParada;
  friend class Bus::ObservadorParada;
  class ObservadorParada : public Observer {
    Bus* pBus;
  public:
    ObservadorParada(Bus* b) : pBus(b) {}
    void update(Observable*, Argument *arg) {
      cout << "Bus " << pBus->numLinea
		  << " en parada numero " <<arg->nParada  <<". Conexiones: ";
	  for(unsigned i=0;i<arg->conexiones.size();i++)
		  if(arg->conexiones[i]!= pBus->numLinea)
			  cout << arg->conexiones[i] <<" ";
	  cout << endl;

    }
  } elObservadorParada;
  
public:
  Bus(unsigned n) : numLinea(n), elObservadorParada(this) {}
  Observer& getObservadorBus() { return elObservadorParada; }
};


int main() {
  Bus elBus1(60), elBus2(60), elBus3(148);
  unsigned conexionesParada1111[] ={60,148,78};
  Parada num1111(1111,3,conexionesParada1111);
  num1111.elNotificardorLineasBuses.addObserver(elBus1.getObservadorBus());
  num1111.elNotificardorLineasBuses.addObserver(elBus2.getObservadorBus());
  num1111.elNotificardorLineasBuses.addObserver(elBus3.getObservadorBus());

  // elBus3 ya no est� interesado en esta parada
  num1111.elNotificardorLineasBuses.deleteObserver(elBus2.getObservadorBus());
  // Notificar las lineas de las paradas
  num1111.cambiosEnParada();

  num1111.elNotificardorLineasBuses.deleteObservers();
  // Nadie se entera
  num1111.cambiosEnParada();

  //int esperar =getchar();
} 
