#include <iostream>
#include <vector>
using namespace std;


class Notificador{
public:
	virtual void notificarBuses() = 0;
};

class Parada {
  unsigned numParada;
  vector<unsigned> conexionesBuses;

public:

  // Clase interna:
  class NotificadorBuses;
  friend class Parada::NotificadorBuses;
  class NotificadorBuses : public Notificador {
    Parada* pParada;
  public:
	  NotificadorBuses(Parada* f) : pParada(f){}
	  void notificarBuses() {
		cout << "Parada " << pParada->numParada <<". Conexiones: ";
		for(unsigned i=0;i<pParada->conexionesBuses.size();i++)
			  cout << pParada->conexionesBuses[i] <<" ";
		cout << endl;
	}

    } elNotificardorLineasBuses;

  Parada(unsigned numP, unsigned numC, unsigned *pConexiones ) : numParada(numP), elNotificardorLineasBuses(this) {
	  for(unsigned i=0;i<numC;i++)
		  conexionesBuses.push_back(pConexiones[i]);

  }
  Notificador* getNotificador() {return &elNotificardorLineasBuses;}

 };


int main() {
  unsigned conexionesParada1111[] ={60,148,78};
  Parada num1111(1111,3,conexionesParada1111);

  Notificador *pNotificador = num1111.getNotificador();
  pNotificador->notificarBuses();


} 