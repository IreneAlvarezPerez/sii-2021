#include <iostream>
using namespace std;

class Caja {
  float saldo;
public:
  Caja() {saldo=1000;}
  void Ingresa (float cantidad) {
    saldo += cantidad;
  }
  void Saca (float cantidad) { saldo -= cantidad;}
  void Imprime () { cout << "Saldo caja: " << saldo << endl;}
};

class Personaje {
  protected: 
    float saldo;
    Personaje *interlocutor;
  public:
    Personaje() {saldo=100;}
    void Conecta(Personaje * p) {interlocutor=p;}
    virtual void Recibe(float cantidad) {saldo+=cantidad;}
    void Imprime() { cout << "Saldo personaje: " << saldo << endl;}
};

class Cajero : public Personaje {
  Caja *pcaja;
public:
  void Gestiona(Caja *c){pcaja = c;}
  void Recibe (float cantidad) {
     pcaja->Ingresa(cantidad);
     pcaja->Saca(cantidad*0.0f);
     saldo+=cantidad*0.05f;
  }
};

class Jugador : public Personaje {
public:
  void Paga (float cantidad) {
    saldo-=cantidad;
    interlocutor->Recibe(cantidad);
  }
};

int main() {
  Cajero cajero;
  Jugador jugador;
  Caja caja;

  cajero.Imprime();
  jugador.Imprime();
  caja.Imprime();

  jugador.Conecta(&cajero);
  cajero.Gestiona(&caja);

  cajero.Imprime();
  jugador.Imprime();
  caja.Imprime();

  jugador.Recibe(300);

  cajero.Imprime();
  jugador.Imprime();
  caja.Imprime();

  jugador.Paga(50);

  cajero.Imprime();
  jugador.Imprime();
  caja.Imprime();
  return 0;
}
