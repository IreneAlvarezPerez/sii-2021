#include <iostream>
#include <string>

class RobotWizo {
public:
    void encenderBluetooth()
    {
        std::cout<< "Bluetooth correctamente encendido\n";
    }
    void apagarBluetooth()
    {
	std::cout<< "Bluetooth apagado\n";
    }
    void mover(float x, float y)
    {
	std::cout<< "Moviendo motores Wizo...\n";
    }
};

class RobotDico {
public:
    bool checkNextPos(float x, float y) {
	std::cout<< "Checking position ...\n";
	return true; // podria devolver false / true
    }
    void go(float x, float y) {
	if (checkNextPos(x, y))   
	  std::cout<< "Going Dico to... " << x << " and " << y << "\n";
	else
	  std::cout<< "Impossible movement\n";
    }
    void connectWifi() {
        std::cout<< "Connected by Wifi\n";
    }
    void disconnectWifi() {
        std::cout<< "Disconnected Wifi\n";
    }
};

class IRobot
{
private:
    float x, y;
public:
    virtual ~IRobot(){}
    virtual void conectar() = 0;
    virtual void mover(float x, float y) = 0;
};

class ARobotWizo : public IRobot {
private:
    RobotWizo robot;
    friend class FactoriaRobots;
    ARobotWizo(){}
public:
    virtual ~ARobotWizo() {
	robot.apagarBluetooth();
    }
    void conectar() {
	robot.encenderBluetooth();
    }
    void mover(float x, float y) {
	robot.mover(x, y);
    }
};

class ARobotDico : public IRobot {
private:
    RobotDico robot;
    friend class FactoriaRobots;
    ARobotDico(){}
public:
    virtual ~ARobotDico() {
	robot.disconnectWifi();
    }
    void conectar() {
	robot.connectWifi();
    }
    void mover(float x, float y) {
        robot.go(x, y);
    }
};

class FactoriaRobots {
   FactoriaRobots(){}
   void operator=(FactoriaRobots&);
   FactoriaRobots(const FactoriaRobots&);
public:
   static FactoriaRobots& getInstancia(){
	static FactoriaRobots unicaInstancia;
	return unicaInstancia;
   }
   IRobot * crearRobot (char tipo) {
	if (tipo == 'Z') {
          return new ARobotWizo();
        }
        else if (tipo == 'D') {
          return new ARobotDico();
        }
	else return NULL;
   }
};

int main(int argc, char* argv[])
{
    IRobot *aRobot = 0;
    char tipo;
    float x, y;
    std::cout << "Introduzca robot a controlar: {Z, D}: ";
    std::cin >> tipo;
    
    aRobot = FactoriaRobots::getInstancia().crearRobot(tipo);

    if(aRobot)
    {
	aRobot->conectar();
    	std::cout << "Introduzca la posicion x: ";
    	std::cin >> x;
    	std::cout << "Introduzca la posicion y: ";
    	std::cin >> y;
	aRobot->mover(x, y);
	delete aRobot;
    }
    return 0;
};
