#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#include <signal.h>

struct Tmensaje{
    int id; 	    // id del sensor
    float temp;	    // valor de temperatura
    long timestamp; // hora actual en microsegundos
};

long getMicrotime() {
     struct timeval horaActual;
     gettimeofday(&horaActual, NULL);
     return horaActual.tv_sec * (int)1e6 + horaActual.tv_usec;
}

int fd;
int tuberia;
pid_t pid;

void tratar(int codigo) {
     printf("Tratando SIGPIPE: %d\n", codigo);
     kill(pid, SIGKILL);
}

int main(int argc, char *argv[])
{
    struct flock fl;
    struct Tmensaje mensaje;
  
    struct sigaction sa;
    pid = getpid();

    // Se abre la tuberia para enviar la informacion
    tuberia = open("/tmp/Tuberia",O_WRONLY);

    fl.l_whence = SEEK_SET; // Desde el comienzo
    fl.l_start = 0;
    fl.l_len = 0; /* Cerrojo sobre todo el fichero */
    fl.l_pid = getpid();

    // Abre el fichero añadiendo los datos al final
    fd = open("sharedlog.txt", O_WRONLY | O_APPEND);
    if (fd < 0) {
        perror("Error abriendo fichero\n");
	return -1;
    }

    sa.sa_handler = &tratar;
    sa.sa_flags = 0;
    if (sigaction(SIGPIPE, &sa, 0) == -1) {
      perror("sigaction");
      exit(1);
    }
 
    while(1)
    {
        fl.l_type = F_WRLCK; /* Cerrojo exclusivo */
        fcntl(fd, F_SETLKW, &fl); /* Servicio bloqueante */
        lseek(fd, 0, SEEK_END);
	mensaje.id = atoi(argv[1]);
        mensaje.temp = rand() % 10; // Valor aleatorio getTemperatura();
	mensaje.timestamp = getMicrotime();
	printf("Sensor ID: %d - %0.2f grados [%lu]\n", mensaje.id, mensaje.temp, mensaje.timestamp);
	write(fd, &mensaje, sizeof(mensaje)); // Se escribe en el fichero 
	write(tuberia, &mensaje, sizeof(mensaje)); // Se envia por la tuberia
	sleep(5); // se simula el tiempo que el cerrojo bloquea el fichero
        fl.l_type = F_UNLCK;
        fcntl(fd, F_SETLKW, &fl); /* Elimina el cerrojo */
        sleep(2); // se simula el tiempo que no esta bloqueado
    }

    close(fd);
    close(tuberia);
    return 0;
}
