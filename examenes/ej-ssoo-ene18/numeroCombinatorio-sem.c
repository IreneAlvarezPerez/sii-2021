// Compilar con opcion: -lpthread

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <semaphore.h>

sem_t semaforo;
int x=1, y=1, n=7, k=4;

void Proceso1() {
	int i;
	for (i=n-k+1; i<=n; i++) {
		x=x*i;
	}
	sem_post(&semaforo);
}

void Proceso2() {
	int i;
	for (i=2; i<=k; i++) {
		y=y*i;		
	}
	sem_wait(&semaforo);
	printf("C(n,k) = %d\n", x/y);
}

int main() {
	pthread_t th1, th2;

	// Inicializo el semáforo a 0
	sem_init(&semaforo, 0, 0);

	pthread_create(&th1, NULL, Proceso1, NULL);
	pthread_create(&th2, NULL, Proceso2, NULL);
	pthread_join(th1, NULL);
	pthread_join(th2, NULL);

	sem_destroy(&semaforo);

	return 0;
}
