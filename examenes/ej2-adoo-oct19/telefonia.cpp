#include <iostream>
#include <string>
#include <vector>
using namespace std;

typedef enum{MOVIL, FIJO} TipoTelefono;

class Telefono{
protected:
	TipoTelefono tipo;
	string numero;
public:
	static Telefono * metodoFabricacion(TipoTelefono,string,string);
	virtual void ver() = 0;
};

class Movil : public Telefono {
	string imei;
	friend class Telefono;
	Movil(TipoTelefono t, string n, string i):imei(i){tipo=t;numero=n;}
public:
	void ver() {
		cout<< " Movil: " << numero << " IMEI: " << imei << endl;
	}

};

class Fijo : public Telefono {
	friend class Telefono;
	Fijo(TipoTelefono t, string n){tipo=t;numero=n;}
public:
	void ver() {
		cout<< " Fijo: " << numero << endl;
	}

};

class Telefonia {
	vector<Telefono *> listado;
public:
	Telefonia(){}
	~Telefonia() {
		for(int i=0;i<listado.size();i++)
			delete listado[i];
	}

	void verTodos() {
		for(int i=0;i<listado.size();i++)
			listado[i]->ver();
	}

	void insertarTelefono(Telefono *t) {
		listado.push_back(t);
	}
};

Telefono * Telefono::metodoFabricacion(TipoTelefono tipo, string numero, string imei="")
{
	if(tipo==MOVIL) return new Movil(tipo, numero, imei);
	else if(tipo==FIJO) return new Fijo(tipo, numero);
	else return 0;
}


int main()
{
	Telefonia telefonos;
	char opcion; // M: movil y F: fijo
	string numero, imei;
	
	cout << "Selecciona: M movil / F fijo / S salir" << endl;
	cin >> opcion;
	while (opcion != 'S') {
	  cout << "Introduce numero de telefono:" << endl;
	  cin >> numero;
	  if (opcion == 'M') {
		cout << "Introduce el IMEI" << endl;
		cin >> imei;
		telefonos.insertarTelefono(Telefono::metodoFabricacion(MOVIL, numero, imei));
          }
	  else if (opcion == 'F')
		telefonos.insertarTelefono(Telefono::metodoFabricacion(FIJO, numero));
	  cout << "Selecciona: M movil / F fijo / S salir" << endl;
	  cin >> opcion;	  
	}
	cout << "Telefonos dados de alta:" << endl;
	telefonos.verTodos();
	return 0;
}

/*

Selecciona: M movil / F fijo / S salir
M
Introduce numero de telefono:
922384
Introduce el IMEI
119199999999
Selecciona: M movil / F fijo / S salir
F
Introduce numero de telefono:
911113939
Selecciona: M movil / F fijo / S salir
S
Telefonos dados de alta:
 Movil: 922384 IMEI: 119199999999
 Fijo: 911113939
*/
