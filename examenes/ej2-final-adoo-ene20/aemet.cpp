#include <iostream>
#include <vector>
using namespace std;

class WeatherData {
   vector <class Observer *> observers;
   float wind; // km/h
 public:
   void attach(Observer *obs) {observers.push_back(obs);}
   void setWind(float w) {
 	wind = w;
 	notifyObservers();
   }
   int getWind() {return wind;}
   void notifyObservers();
};

class Observer {
 public:
   virtual void update(float wind) = 0;
};

void WeatherData::notifyObservers() {
 for (int i = 0; i < observers.size(); i++)
   observers[i]->update(wind);
}

class DGTObserver: public Observer {
 public:
   void update(float wind) {
 	cout << "-- DGT: wind " << wind << '\n';
	if (wind > 100)
	  cout << "Alert Wind - Level 5" << '\n';
 }
};

class AENAObserver: public Observer {
 public:
   void update(float wind) {
	cout << "-- AENA: wind " << wind << '\n';
	if (wind > 200)
	  cout << "Risk Wind - Maximum" << '\n';
   }
};

class ADIFObserver: public Observer {
 public:
   void update(float wind) {
	cout << "-- ADIF: wind " << wind << '\n';
	if (wind > 150)
	  cout << "Precaution Wind" << '\n';
   }
};

int main() {
 WeatherData aemet;
 DGTObserver dgt;
 AENAObserver aena;
 //ADIFObserver adif;
 
 aemet.attach(&dgt);
 aemet.attach(&aena);
 //aemet.attach(&adif);
 aemet.setWind(120);
}
